// ==UserScript==
// @name         Shortcut keys for acloudguru
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Shortcut keys for acloud.guru site
// @author       Ian
// @match        https://acloud.guru/course/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.onkeydown = function(e) {
        if (e.code === 'ArrowUp') {
            $('button.playbackValue').click();
        } else if (e.code === 'ArrowLeft') {
            $('skip-buttons button')[0].click();
        } else if (e.code === 'ArrowRight') {
            $('skip-buttons button~button').click();
        } else if (e.code === 'Space') {
            var pause = $$('button.pause')[0];
            if (pause) {
                pause.click();
            } else {
                $('button.play').click();
            }
        }
    };
    console.log('Shortcut keys mod loaded!');
})();